#!/bin/bash

#Test if the yaml has more than 1 group
groups=$(cat modelo.yml | yq .groups[1])
echo "Groups: $groups"
boolGroups=0
boolMicroservices=0
boolRoutes=0
boolrateLimitConfig=0
bool=0
if [ "$groups" == "null" ]
then
	echo "Ok, the yaml has only one group."
else
	echo "ERROR: The yaml has more than one group"
	boolGroups=1
	bool=1	
fi

#Test if the yaml has microservices.
microservices=$(cat modelo.yml | yq .groups[0].microservices[0])
echo "Microservices: $microservices"
if [ "$microservices" == "null" ]
then
	echo "ERROR: The groups must have one microservice or more"
	boolMicroservices=1
	bool=1
else
	echo "Ok, the group has one microservice or more"
fi	

#Test if the microservices have routes and rateLimitConfig
i=0
while [ "$microservices" != "null" ]
do
	
	routes=$(cat modelo.yml | yq .groups[$contador].microservices[$i].routes[0])
	echo "Microservices: $microservices"
	echo "Routes: $routes"
	if  [ "$routes" == "null" ]
	then
		echo "ERROR: The microservice must have a route"
		boolMicroservices=1
		bool=1
	fi
	j=0
	rateLimitConfig=$(cat modelo.yml | yq .groups[$contador].microservices[$i].rateLimitConfig[$j])
	echo "rateLimitConfig: $rateLimitConfig"
	while [ "$rateLimitConfig" != "null" ]
	do
		restriction=$(cat modelo.yml | yq .groups[$contador].microservices[$i].rateLimitConfig[$j].restriction[1])
		if [ "$restriction" != "null" ]; then
			echo "Error: each rateLimitConfig can only have one restriction"
			boolrateLimitConfig=1
			bool=1
		fi
	j=$((j+1))
	rateLimitConfig=$(cat modelo.yml | yq .groups[$contador].microservices[$i].rateLimitConfig[$j])
	done
	i=$((i+1))
	microservices=$(cat modelo.yml | yq .groups[0].microservices[$i])
done


if [ "$bool" == "1" ]
then
	echo "ERROR: The yaml file has this errors: "
	if [ "$boolGroups" == "1" ]
	then
		echo "- Has more than 1 group"
	fi

	if [ "$boolMicroservices" == "1" ]
	then
		echo "- Does not have a microservice"
	fi
	
	if [ "$boolRoutes" == "1" ]
	then
		echo "- One or more of the microservices do not have a route"
	fi

	if [ "$boolrateLimitConfig" == "1" ]
	then
		echo "- One or more of the rateLimitConfig have more than 1 restriction"
	fi
	exit 1
else
#The yaml is OK
	echo "SUCCESS: Zuul yaml validated"
	exit 0
fi
